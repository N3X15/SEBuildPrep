﻿using NDesk.Options;
using SEBuildPrep.VisualStudio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace SEBuildPrep
{
    class Program
    {
        static bool BatchMode=false;
        static void Main(string[] args)
        {
            var opts = new OptionSet() {
                {"b|batch", "Enable batch mode, which disables prompts.", (v) => BatchMode=true },
            };
            opts.Parse(args);
            Console.WriteLine("SEBuildPrep v" + Assembly.GetCallingAssembly().GetName().Version.ToString());
            Console.WriteLine(" Copyright (c)2016 Rob \"N3X15\" Nelson. Available under the MIT License.");
            Console.WriteLine("___________________________________");
            Console.WriteLine();
            Console.WriteLine("Performing preflight...");
            if(!CheckOKFail("Checking for config.yml...", () => File.Exists("config.yml")))
            {
                Console.WriteLine("   config.yml is required.");
                PressAnyKey();
                return;
            }
            if (!CheckOKFail("Checking for user-config.yml...", () => File.Exists("user-config.yml")))
            {
                Console.WriteLine("   user-config.yml is required. Please edit and rename user-config.yml.example.");
                PressAnyKey();
                return;
            }
            var config = new YAMLConfig();
            config.LoadFromFile("config.yml");
            config.SaveToFile("config.read.yml");
            var userconfig = new YAMLConfig();

            userconfig.LoadFromFile("user-config.yml");
            var newrefs = config.Get<YamlSequenceNode>("reference-fixes", new YamlSequenceNode());

            var STOCK_BIN_DIR = userconfig.Get("SECE.Paths.STOCK_BIN_DIR");
            foreach (var projectDir in Directory.GetDirectories("Sources"))
            {
                var csprojFile = Directory.GetFiles(projectDir, "*.csproj").First();
                Console.WriteLine("Fixing {0}...", csprojFile);
                var project = new VS2015Project();
                project.LoadFromFile(csprojFile);
                int nFixedRefs = 0;
                int nRemovedRefs = 0;
                if (csprojFile.EndsWith("VRage.Network.csproj"))
                {
                    int patched = 0;
                    foreach(var csFile in project.Files)
                    {
                        if (csFile.Include.Contains("MyRakNet"))
                        {
                            csFile.Condition = "('$(Configuration)' == 'Debug_XB1') Or ('$(Configuration)' == 'Release_XB1')";
                            patched++;
                        }
                    }
                    Console.WriteLine("  Fixed {0}/{1} <Compile> entries.", patched, project.Files.Count);
                }
                foreach (var refIDNode in newrefs)
                {
                    var refID = "";
                    YamlMappingNode cfgNode = null;
                    switch (refIDNode.NodeType)
                    {
                        case  YamlNodeType.Scalar:
                            // - RakNet
                            refID = ((YamlScalarNode)refIDNode).Value;
                            break;
                        case YamlNodeType.Mapping:
                            // - RakNet:
                            //     delete: true/false
                            //     Condition: ...
                            var map = ((YamlMappingNode)refIDNode).First();
                            refID = ((YamlScalarNode)map.Key).Value;
                            cfgNode = (YamlMappingNode)map.Value;
                            break;
                    }
                    if (project.HasReference(refID))
                    {
                        var refInfo = project.GetReference(refID);
                        var newHintPath = Path.Combine(STOCK_BIN_DIR, refID + ".dll");
                        if (cfgNode != null && cfgNode.Children.ContainsKey("delete"))
                        {
                            if ((cfgNode["delete"] as YamlScalarNode).Value == "true")
                            {
                                nRemovedRefs += project.RemoveReference(refID);
                                continue;
                            }
                        }
                        if (!(refInfo is AssemblyReference) || (refInfo as AssemblyReference).HintPath != newHintPath)
                        {
                            nFixedRefs++;
                            nRemovedRefs += project.RemoveReference(refID) - 1;
                            refInfo = project.AddAssemblyRef(refID, newHintPath);
                            //Console.WriteLine("  {0} -> {1}", refInfo, newRefInfo);
                        }
                        if (cfgNode != null) {
                            foreach (var kvp in cfgNode)
                            {
                                var key = ((YamlScalarNode)kvp.Key).Value;
                                var value = ((YamlScalarNode)kvp.Value).Value;
                                var prop = refInfo.GetType().GetProperty(key);
                                if (prop.PropertyType == typeof(string))
                                {
                                    prop.SetValue(refInfo, value);
                                }
                                if (prop.PropertyType == typeof(bool))
                                {
                                    prop.SetValue(refInfo, value == "true");
                                }
                            }
                        }
                    }
                }
                nRemovedRefs += project.RemoveDuplicates();
                if (nFixedRefs > 0)
                    Console.WriteLine("  Fixed {0}/{1} references.", nFixedRefs, project.ReferencesByName.Count);
                if(nRemovedRefs > 0)
                    Console.WriteLine("  Removed {0} references.", nRemovedRefs);
                project.SaveToFile(csprojFile);
            }
            
            Console.WriteLine("Done!");
            PressAnyKey();
        }

        public static void PressAnyKey()
        {
            if (!BatchMode)
            {
                var fgColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(">>> ");
                Console.ForegroundColor = fgColor;
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey(true);
                Console.WriteLine();
            }
        }

        private static bool CheckOKFail(string v, Func<bool> p)
        {
            return _Check(v, p, "....", " OK ", "FAIL");
        }

        private static bool CheckYN(string v, Func<bool> p)
        {
            return _Check(v, p, "...", " Y ", " N ");
        }
        private static bool _Check(string v,Func<bool> p,string wait, string good, string bad) { 
            Console.Write(wait+v);
            Console.CursorLeft = 0;
            var fgColor = Console.ForegroundColor;
            bool res;
            Console.Write("[");
            if (res = p())
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(good);
            } else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(bad);

            }
            Console.ForegroundColor = fgColor;
            Console.Write("] ");
            Console.WriteLine(v);
            return res;
        }
    }
}
