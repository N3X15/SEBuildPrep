# SEBuildPrep
This tool prepares a Space Engineers source code repository for build by fixing references to assemblies and adding conditions to files that should not be built.

Originally made for Space Engineers: Community Edition (SECE).

# Usage

1. Copy SEBuildPrep.exe, config.yml, and user-config.yml.example to your SE repo's root directory.
2. Rename user-config.yml.example to user-config.yml and edit the STOCK_BIN_DIR path, as directed.
3. Run SEBuildPrep.exe.

Your VS2015 projects should now be properly configured. Build in Release mode and x64 platform.

# Compiling

1. Requires NuGet and VS2015 CE.
2. Restore NuGet packages.
3. Build in Release mode.