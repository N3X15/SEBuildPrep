﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using YamlDotNet.Serialization;

namespace SEBuildPrep.VisualStudio
{
    public class AssemblyReference : BaseReference
    {

        public AssemblyReference(VS2015Project p, XElement refXML) : base(p, refXML) { }

        public string Include
        {
            get
            {
                return GetAttributeValue(wrapped, "Include");
            }
            set
            {
                SetAttributeValue(wrapped, "Include", value);
            }
        }

        public bool Private
        {
            get
            {
                return GetElementValue(wrapped, "Private") == "true";
            }
            set
            {
                SetElementValue(wrapped, "Private", value ? "true" : "false");
            }
        }

        public string HintPath
        {
            get
            {
                return GetElementValue(wrapped, "HintPath");
            }
            set
            {
                SetElementValue(wrapped, "HintPath", value);
            }
        }
        
        [XmlIgnore]
        [YamlIgnore]
        public override string RefID
        {
            get
            {
                return Include.Split(',').First().Trim();
            }
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(HintPath))
                return string.Format("[{0}]", Include);
            return string.Format("[{0} @ {1}]",Include,HintPath);
        }
    }
}
