﻿using System;
using System.Xml.Linq;

namespace SEBuildPrep.VisualStudio
{
    public class BaseFileOperation : BaseProjectElement
    {
        public BaseFileOperation(VS2015Project p, XElement el) : base(p, el) { }

        public OperationType Operation
        {
            get
            {
                return (OperationType)Enum.Parse(typeof(OperationType), wrapped.Name.LocalName);
            }
            set
            {
                wrapped.Name = XName.Get(value.ToString());
            }
        }
        //<Compile Include = "SyncLayer\MySyncedVector3D.cs" />
        public string DependentUpon
        {
            get
            {
                return GetElementValue(wrapped, "DependentUpon");
            }
            set
            {
                SetElementValue(wrapped, "DependentUpon", value);
            }
        }
        public string AutoGen
        {
            get
            {
                return GetElementValue(wrapped, "DependentUpon");
            }
            set
            {
                SetElementValue(wrapped, "DependentUpon", value);
            }
        }

        public bool? Visible
        {
            get
            {
                return GetElementValue(wrapped, "Visible") != "false";
            }
            set
            {
                SetElementValue(wrapped, "Visible", (value == null) ? null : (value.Value ? "true" : "false"));
            }

        }
        //<CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
        public CopyToOutputDirectoryMode? CopyToOutputDirectory
        {
            get
            {
                var val = GetElementValue(wrapped, "CopyToOutputDirectory");
                if (val == null) return null;
                return (CopyToOutputDirectoryMode)Enum.Parse(typeof(CopyToOutputDirectoryMode), val);
            }
            set
            {
                SetElementValue(wrapped, "CopyToOutputDirectory", value == null ? null : value.ToString());
            }
        }

        public string Include
        {
            get
            {
                return GetAttributeValue(wrapped, "Include");
            }
            set
            {
                SetAttributeValue(wrapped, "Include", value);
            }
        }
    }

    public class CompileOperation : BaseFileOperation
    { 
        public CompileOperation(VS2015Project p, XElement el) : base(p, el) { }

    }

    public class EmbeddedResourceOperation : BaseFileOperation
    {
        public EmbeddedResourceOperation(VS2015Project p, XElement el) : base(p, el) { }



        public string Generator
        {
            get
            {
                return GetElementValue(wrapped, "Generator");
            }
            set
            {
                SetElementValue(wrapped, "Generator", value);
            }
        }
        public string LastGenOutput
        {
            get
            {
                return GetElementValue(wrapped, "LastGenOutput");
            }
            set
            {
                SetElementValue(wrapped, "LastGenOutput", value);
            }
        }
        public string CustomToolNamespace
        {
            get
            {
                return GetElementValue(wrapped, "CustomToolNamespace");
            }
            set
            {
                SetElementValue(wrapped, "CustomToolNamespace", value);
            }
        }
        public string LogicalName
        {
            get
            {
                return GetElementValue(wrapped, "LogicalName");
            }
            set
            {
                SetElementValue(wrapped, "LogicalName", value);
            }
        }
        public string Link
        {
            get
            {
                return GetElementValue(wrapped, "Link");
            }
            set
            {
                SetElementValue(wrapped, "Link", value);
            }
        }
    }

    public enum CopyToOutputDirectoryMode
    {
        Always,
        Never,
        PreserveNewest
    }
}