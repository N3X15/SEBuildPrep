﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SEBuildPrep.VisualStudio
{
    public abstract class BaseProjectElement
    {
        protected XElement wrapped;
        //protected VS2015Project project;

        public BaseProjectElement() { }
        public BaseProjectElement(VS2015Project p, XElement x)
        {
            //project = p;
            wrapped = x;
        }

        [XmlAttribute]
        [DefaultValue("")]
        public string Condition {
            get
            {
                return GetAttributeValue(wrapped, "Condition");
            }
            set
            {
                SetAttributeValue(wrapped, "Condition", value);
            }
        }

        protected XName createName(string localName)
        {
            return wrapped.GetDefaultNamespace().GetName(localName);
        }
        public string GetAttributeValue(XElement e, string name, string defaultVal = null)
        {
            var attr = e.Attribute(createName(name));
            if (attr == null)
                attr = e.Attribute(XName.Get(name));
            if (attr == null)
                return defaultVal;
            return attr.Value;
        }

        public void SetAttributeValue(XElement e, string name, string value)
        {
            e.SetAttributeValue(XName.Get(name), value);
        }
        public string GetElementValue(XElement e, string name, string defaultVal = null)
        {
            var attr = e.Element(createName(name));
            if (attr == null)
                attr = e.Element(XName.Get(name));
            if (attr == null)
                return defaultVal;
            return attr.Value;
        }

        public void SetElementValue(XElement e, string name, string value)
        {
            e.SetElementValue(XName.Get(name), value);
        }

        public void Remove()
        {
            wrapped.Remove();
        }
    }
}
