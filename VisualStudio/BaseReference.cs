﻿using System.Xml.Linq;

namespace SEBuildPrep.VisualStudio
{
    public abstract class BaseReference : BaseProjectElement
    {
        public BaseReference(VS2015Project p, XElement refXML) : base(p, refXML) { }
        public abstract string RefID { get; }
    }
}