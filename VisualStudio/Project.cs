﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SEBuildPrep.VisualStudio
{
    public class VS2015Project
    {
        public List<PropertyGroup> PropertyGroups = new List<PropertyGroup>();

        private XElement mReferenceGroup;
        private XElement mProjectReferenceGroup;
        public Dictionary<string, BaseReference> ReferencesByName = new Dictionary<string, BaseReference>();
        public List<BaseReference> References = new List<BaseReference>();

        public List<BaseFileOperation> Files = new List<BaseFileOperation>();


        private XDocument mProject;
        private XNamespace mNS;
        private XElement compileItemGroup;
        private XElement noneItemGroup;
        private XElement embeddedResourceItemGroup;


        public XName createName(string name)
        {
            //return XName.Get(name);
            return mNS.GetName(name);
        }
        public void LoadFromFile(string filename)
        {
            this.mProject = XDocument.Load(filename);
            ReloadEverything();
        }
        private void ReloadEverything()
        {
            mNS = mProject.Root.GetDefaultNamespace();
            ReferencesByName = new Dictionary<string, BaseReference>();
            References = new List<BaseReference>();
            mReferenceGroup = null;
            mProjectReferenceGroup = null;
            PropertyGroups = new List<PropertyGroup>();
            Files = new List<BaseFileOperation>();
            foreach (var node in mProject.Descendants())
            {
                if (node.NodeType == XmlNodeType.Element)
                {
                    var refXML = (XElement)node;
                    switch(refXML.Name.LocalName)
                    {
                        case "Reference":
                            var reference = new AssemblyReference(this, refXML);
                            ReferencesByName[reference.RefID] = reference;
                            References.Add(reference);
                            mReferenceGroup = refXML.Parent;
                            break;
                        case "ProjectReference":
                            var projectReference = new ProjectReference(this, refXML);
                            ReferencesByName[projectReference.RefID] = projectReference;
                            References.Add(projectReference);
                            mProjectReferenceGroup = refXML.Parent;
                            break;
                        case "PropertyGroup":
                            var propertyGroup = new PropertyGroup(this, refXML);
                            PropertyGroups.Add(propertyGroup);
                            break;
                        case "Compile":
                            compileItemGroup = refXML.Parent;
                            Files.Add(new CompileOperation(this, refXML));
                            break;
                        case "None":
                            noneItemGroup = refXML.Parent;
                            Files.Add(new BaseFileOperation(this, refXML));
                            break;
                        case "EmbeddedResource":
                            embeddedResourceItemGroup = refXML.Parent;
                            Files.Add(new EmbeddedResourceOperation(this, refXML));
                            break;
                    }
                }
            }
        }

        public void SaveToFile(string filename)
        {
            foreach (XElement e in mProject.Root.DescendantsAndSelf())
            {
                e.Name = mProject.Root.GetDefaultNamespace() + e.Name.LocalName;
            }
            mProject.Save(filename, SaveOptions.OmitDuplicateNamespaces);
            //var str = mProject.ToString(SaveOptions.OmitDuplicateNamespaces);
            //str=str.Replace(" xmlns=\"\"", "");
            //var doc = new XmlDocument();
            //doc.Load(filename);
            //doc.SelectNodes("//[@xmlns='']")
            //doc.Save(filename);
        }
        public bool HasReference(string refid)
        {
            return ReferencesByName.ContainsKey(refid) || References.Find(x => x.RefID==refid)!=null;
        }

        public AssemblyReference AddAssemblyRef(string refID, string hintpath, bool verbose = false)
        {
            if (verbose)
                Console.WriteLine(string.Format("Adding assembly reference {0}.", refID));
            var reference = subelement(mReferenceGroup, "Reference");
            var asmRef = new AssemblyReference(this, reference);
            asmRef.Include = refID;
            asmRef.HintPath = hintpath;
            ReferencesByName[asmRef.RefID] = asmRef;
            References.Add(asmRef);
            return asmRef;
        }

        public ProjectReference AddProjectRef(string include, string name, string guid, bool verbose = false)
        {
            if (verbose)
                Console.WriteLine(string.Format("Adding project reference {0}.", name));
            var reference = subelement(mProjectReferenceGroup, "ProjectReference");
            var projRef = new ProjectReference(this, reference);
            ReferencesByName[projRef.RefID] = projRef;
            References.Add(projRef);
            projRef.Name = name;
            projRef.Include = include;
            projRef.Project = guid;
            return projRef;
        }

        public BaseReference GetReference(string refID)
        {
            return ReferencesByName[refID];
        }

        public int RemoveReference(string refID)
        {
            int nRemoved = 0;
            if (ReferencesByName.ContainsKey(refID))
                ReferencesByName.Remove(refID);
            foreach (var reference in (new List<BaseReference>(References)).FindAll((x) => x.RefID == refID))
            {
                reference.Remove();
                References.Remove(reference);
                nRemoved++;
            }
            return nRemoved;
        }

        public int RemoveDuplicates()
        {
            int nRemoved = 0;
            foreach (var refID in ReferencesByName.Keys)
            {
                foreach (var reference in (new List<BaseReference>(References)).FindAll((x) => x.RefID == refID))
                {
                    if (reference == ReferencesByName[refID]) continue;
                    reference.Remove();
                    References.Remove(reference);
                    nRemoved++;
                }
            }
            return nRemoved;
        }

        private XElement subelement(XElement referenceGroup, string tag)
        {
            var child = new XElement(XName.Get(tag));
            referenceGroup.Add(child);
            return child;
        }
    }
}