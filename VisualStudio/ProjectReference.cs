﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SEBuildPrep.VisualStudio
{
    public class ProjectReference:BaseReference
    {
        public ProjectReference(VS2015Project p, XElement refXML) : base(p, refXML) { }

        public string Include
        {
            get
            {
                return GetAttributeValue(wrapped, "Include");
            }
            set
            {
                SetAttributeValue(wrapped, "Include", value);
            }
        }

        public string Project
        {
            get
            {
                return wrapped.Element(createName("Project")).Value;
            }
            set
            {
                wrapped.SetElementValue(createName("Project"),value);
            }
        }


        public string Name
        {
            get
            {
                return wrapped.Element(createName("Name")).Value;
            }
            set
            {
                wrapped.SetElementValue(createName("Name"), value);
            }
        }

        public override string RefID
        {
            get
            {
                return string.IsNullOrEmpty(Name) ? Include:Name;
            }
        }

        public override string ToString()
        {
            return string.Format("[{0};{1}]",Include,Project);
        }
    }
}
