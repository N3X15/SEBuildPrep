﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace SEBuildPrep.VisualStudio
{
    public class PropertyGroup : BaseProjectElement
    {

        public PropertyGroup(VS2015Project p, XElement refXML):base(p,refXML) { }

        public string this[string key]
        {
            get
            {
                var el = wrapped.Element(createName(key));
                if (el == null) return null;
                return el.Value;
            }
            set
            {
                wrapped.SetElementValue(createName(key), value);
            }
        }
    }
}