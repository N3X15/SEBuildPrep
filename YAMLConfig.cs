﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Core;
using YamlDotNet.RepresentationModel;

namespace SEBuildPrep
{
    class YAMLConfig
    {
        private YamlMappingNode m_root;

        public void LoadFromFile(string filename)
        {
            try
            {
                using (var rdr = File.OpenText(filename))
                {
                    var stream = new YamlStream();
                    stream.Load(rdr);
                    m_root = (YamlMappingNode)stream.First().RootNode;
                }
            }
            catch (SyntaxErrorException e)
            {
                Console.WriteLine("ERROR while loading {0}:", filename);
                Console.WriteLine(e.Message);
                Program.PressAnyKey();
                Environment.Exit(1);
            }
        }

        public void SaveToFile(string filename)
        {
            using(var writer = File.CreateText(filename))
            {
                var stream = new YamlStream();
                stream.Add(new YamlDocument(m_root));
                stream.Save(writer,false);
            }
        }

        public T Get<T>(string path, T defaultValue = default(T)) where T:YamlNode
        {
            return (T)delimget(m_root, path, defaultValue);
        }

        public string Get(string path, string defaultValue="")
        {
            return Get<YamlScalarNode>(path, new YamlScalarNode(defaultValue)).Value;
        }

        public YamlNode this[string key]
        {
            get
            {
                return m_root[key];
            }
        }

        public void Set(string path, string value)
        {
            delimset(m_root, path, new YamlScalarNode(value));
        }
        private object delimget(YamlMappingNode cfg, string key, object defaultValue = null, char delim = '.') {
            var parts = key.Split(delim);
            try {
                var value = cfg[parts[0]];
                if (parts.Count() == 1)
                    return value;
                foreach (var part in parts.Skip(1))
                    value = value[part];
                return value;
            } catch (KeyNotFoundException) {
                return defaultValue;
            }
        }

        private void delimset(YamlMappingNode cfg, string key, YamlScalarNode value, char delim = '.')
        {
            var parts = key.Split(delim);
            if (parts.Count() == 1)
                cfg.Children[parts[0]] = value;
            if (!cfg.Children.ContainsKey(parts[0]))
                cfg.Children[parts[0]] = new YamlMappingNode();
            var L = cfg[parts[0]] as YamlMappingNode;
            foreach (var part in parts.Slice(1, -1))
            {
                if (!L.Children.ContainsKey(part))
                    L.Children[part] = new YamlMappingNode();
                L = L[part] as YamlMappingNode;
            }
            L.Children[parts.Last()] = value;
        }
    }
}
